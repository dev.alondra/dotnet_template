using System.ComponentModel.DataAnnotations;

namespace Demo.Core.model
{
    public class DemoModel
    {
        [Key]
        public int Id { get; set; }
        public string MyProperty { get; set; }
    }
}