using Demo.Core.model;
using Microsoft.EntityFrameworkCore;

namespace Demo.Data.data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            
        }

        public DbSet<DemoModel> DemoModels { get; set; }
    }
}