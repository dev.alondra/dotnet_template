using Demo.Data.data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.Api.Extensions
{
    public static class ServicesExtension
    {
        public static IServiceCollection AddServiceContext(this IServiceCollection services, IConfiguration config) 
        {
            services.AddDbContext<DataContext>(option => {
                var connectionString = config.GetConnectionString("DefaultConnection");
                var mySqlVersion = new MySqlServerVersion(ServerVersion.AutoDetect(connectionString));

                option.UseMySql(connectionString, mySqlVersion);
                option.UseSnakeCaseNamingConvention();
            });

            return services;
        }
    }
}